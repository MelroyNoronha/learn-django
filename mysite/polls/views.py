from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.shortcuts import render

from .models import Question

# Create your views here.
def index(request):
  latest_5_questions = Question.objects.order_by('pub_date')[:5]
  context = {
    'latest_5_questions': latest_5_questions,
  }

  return render(request, 'polls/index.html', context)

def detail(request, question_id):
  question = get_object_or_404(Question, pk=question_id)
  return render(request, 'polls/detail.html', { 'question': question })

def results(request, question_id):
  return HttpResponse("You're looking at the results of of question %s." % question_id)

def vote(request, question_id):
  return HttpResponse("You're voting on question %s." % question_id)


# Notes:
# The render() function takes the request object as its first argument, 
# a template name as its second argument and a dictionary as its optional third
# argument. 
# It returns an HttpResponse object of the given template rendered with the 
# given context.
