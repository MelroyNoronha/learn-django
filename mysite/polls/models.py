from django.db import models
from django.utils import timezone
import datetime

# Create your models here.

class Question(models.Model):
  question_text = models.CharField('question asked to the voter', max_length=200)
  pub_date = models.DateTimeField('date published')

  def __str__(self):
    return self.question_text

  def was_published_recently(self):
    return self.pub_date >= timezone.now() - datetime.timedelta(days = 1)

class Choice(models.Model):
  question  = models.ForeignKey(Question, on_delete=models.CASCADE)
  choice_text = models.CharField(max_length=200)
  votes = models.IntegerField(default=0)

  def __str__(self):
    return self.choice_text

class Task(models.Model):
  title = models.CharField('Title', max_length=200)
  description = models.CharField('Description', max_length=2000)

  OPEN = 'OP'
  COMPLETED = 'CO'
  IN_PROGRESS = 'IP'
  ON_HOLD = 'OH'

  STATUS_CHOICES = [
    (OPEN, 'Open'),
    (COMPLETED, 'Completed'),
    (IN_PROGRESS, 'In progress'),
    (ON_HOLD, 'On Hold'),
  ]

  status= models.CharField(max_length=2, choices=STATUS_CHOICES, default=OPEN)