from django.contrib import admin
from .models import Question, Task, Choice

# Register your models here.

admin.site.register(Question)
admin.site.register(Task)
admin.site.register(Choice)